# AMC7135 - LED Driver Breakout v1.0 #

## Schematic ##

![Schematic.png](https://bytebucket.org/serisman/pcb-amc7135-breakout/raw/master/output/Schematic.png)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.3" x 0.4" (7.65 mm x 10.21 mm)
* $0.20 each ($0.60 for 3)
* [https://oshpark.com/shared_projects/OI21xxjl](https://oshpark.com/shared_projects/OI21xxjl)

### PCB Front ###

![PCB - Front.png](https://bytebucket.org/serisman/pcb-amc7135-breakout/raw/master/output/PCB%20-%20Front.png)

### PCB Back ###

![PCB - Back.png](https://bytebucket.org/serisman/pcb-amc7135-breakout/raw/master/output/PCB%20-%20Back.png)